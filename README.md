Product API service using NestJS, NestJS/Crud, Typeorm etc.

## Start

```
npm start
```

## Build

```
npm run build
```

## Start development

```
npm run start:dev
```

## Run migration

```
npm run migration:run
```

## Generate migration

```
npm run migration:generate [NAME]
```

## Environment variables:
* PORT - application port 
* DB_HOST - database host
* DB_USERNAME - database user 
* DB_PASSWORD - database password
* DB_NAME - database name
* DB_PORT - database port 

