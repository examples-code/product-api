import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Account } from './account.entity';
import { WhereAccount } from './account.interface';

@Injectable()
export class AccountService {
  constructor(
    @InjectRepository(Account) private accountRepository: Repository<Account>,
  ) {}

  getOne(where: WhereAccount): Promise<Account | undefined> {
    return this.accountRepository.findOne(where);
  }
}
