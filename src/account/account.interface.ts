import { Account } from './account.entity';

export type WhereAccount = Partial<LoggedAccount>;
export type LoggedAccount = Omit<Account, 'password'>;
