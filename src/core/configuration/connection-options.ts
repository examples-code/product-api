import { join } from 'path';

export default {
    type: 'mysql',
    username: process.env.DB_USERNAME,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: parseInt(process.env.DB_PORT, 10),
    entities: [join(__dirname + './../../**/*.entity{.ts,.js}')],
    migrationsTableName: "migrations",
    migrations: [join(__dirname + './../../migrations/**/*{.ts,.js}')],
    cli: {
        "migrationsDir":['src/migrations'],
    }
}