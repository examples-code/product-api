import connectionOptions from './connection-options';

export default () => ({
  PORT: parseInt(process.env.PORT, 10) || 3000,
  DB: connectionOptions,
  JWT_SECRET: process.env.JWT_SECRET || 'secret',
});
