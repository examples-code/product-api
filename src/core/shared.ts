import * as crypto from 'crypto';

export const getPasswordHash = (password: string): string =>
  crypto.createHmac('sha256', String(password)).digest('hex');

export const isUndefined = (item: any): item is undefined =>
  typeof item == 'undefined';

export const isEmpty = (obj: any): obj is null | undefined =>
  isUndefined(obj) || obj === null;
