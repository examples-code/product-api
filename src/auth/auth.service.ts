import { Injectable } from '@nestjs/common';
import { AccountService } from '../account/account.service';
import { getPasswordHash } from '../core/shared';
import { JwtService } from '@nestjs/jwt';
import { isEmpty } from '../core/shared';
import { LoggedAccount } from '../account/account.interface';

@Injectable()
export class AuthService {
  constructor(
    private accountService: AccountService,
    private jwtService: JwtService,
  ) {}

  async validate(
    email: string,
    password: string,
  ): Promise<LoggedAccount | null> {
    const account = await this.accountService.getOne({
      email,
    });

    if (!isEmpty(account) && account.password === getPasswordHash(password)) {
      const { password, ...data } = account;
      return data;
    }
    return null;
  }

  generateJWT(data: LoggedAccount): string {
    return this.jwtService.sign(data, { expiresIn: '1h' });
  }
}
