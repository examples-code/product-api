import {
  Body,
  Controller,
  Post,
  UsePipes,
  ValidationPipe,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';
import { isEmpty } from '../core/shared';
import {
  ApiCreatedResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { LoginSuccessDto } from './dto/login-success.dto';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiCreatedResponse({
    description: 'Account is logged success.',
    type: LoginSuccessDto,
  })
  @ApiUnauthorizedResponse({
    description: 'Login is failed.',
    type: UnauthorizedException,
  })
  @Post('/login')
  @UsePipes(new ValidationPipe({ transform: true }))
  async login(@Body() body: LoginDto): Promise<LoginSuccessDto> {
    const account = await this.authService.validate(body.email, body.password);

    if (isEmpty(account)) {
      throw new UnauthorizedException();
    }

    return new LoginSuccessDto({
      accessToken: this.authService.generateJWT(account),
    });
  }
}
