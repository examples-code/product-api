import { ApiResponseProperty } from '@nestjs/swagger';

export class LoginSuccessDto {
  @ApiResponseProperty()
  accessToken: string;

  constructor(partial: Partial<LoginSuccessDto>) {
    Object.assign(this, partial);
  }
}
