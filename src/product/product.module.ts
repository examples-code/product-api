import { Module, CacheModule } from '@nestjs/common';
import { ProductService } from './product.service';
import { ProductController } from './product.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Product } from './product.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([Product]),
    CacheModule.register({
      ttl: 5,
      max: 10,
    }),
  ],
  providers: [ProductService],
  controllers: [ProductController],
})
export class ProductModule {}
