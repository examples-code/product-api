import {
  CacheInterceptor,
  CacheTTL,
  Controller,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { JwtAuthGuard } from '../core/guards/jwt-auth.guard';
import { CACHE_TTL, PAGE_SIZE } from './product.const';
import { Product } from './product.entity';
import { ProductService } from './product.service';

@Crud({
  model: {
    type: Product,
  },
  dto: {
    create: Product,
    update: Product,
    replace: Product,
  },
  routes: {
    createOneBase: {
      decorators: [UseGuards(JwtAuthGuard)],
    },
    replaceOneBase: {
      decorators: [UseGuards(JwtAuthGuard)],
    },
    updateOneBase: {
      decorators: [UseGuards(JwtAuthGuard)],
    },
    getOneBase: {
      decorators: [CacheTTL(CACHE_TTL)],
    },
  },
  query: {
    limit: PAGE_SIZE,
    alwaysPaginate: true,
  },
})
@UseInterceptors(CacheInterceptor)
@Controller('product')
export class ProductController implements CrudController<Product> {
  constructor(public service: ProductService) {}
}
